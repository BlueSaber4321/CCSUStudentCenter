# How to report lost items using Crowdfind

{% embed url="https://central-connecticut-state-university-student-center.v2.crowdfind.com/login" %}
Crowdfind Login
{% endembed %}

<details>

<summary>Have you signed up for CrowdFind? Read this first if you haven't.</summary>

Head to your Student Center email and look for an email from Crowdfind, by searching in the top search bar. Click on the link where it says, "click here", and it will take you to a page where you can input a password. After that, you are all set, and your account is activated.

</details>

### Report a lost item on Crowdfind

1. Head to the [Crowdfind Page here](https://central-connecticut-state-university-student-center.v2.crowdfind.com)
2.  Log in with your Student Center account email and Password

    ![](<.gitbook/assets/image (17).png>)
3.  Depending on where the item was located, change the location here.

    ![](<.gitbook/assets/image (27).png>)![](<.gitbook/assets/image (29).png>)
4.  Click the Items button

    ![](<.gitbook/assets/image (32).png>)
5.  Click the + button in the bottom right

    ![](<.gitbook/assets/image (33).png>)
6.  Fill out the form, provide a picture if possible, and give a brief explanation of what the item is. In internal notes, leave where the item was found, as well as when it was found.

    ![](<.gitbook/assets/image (6).png>)
7. Press the "Save" button if there was only one item found or "Save + Add" if there are multiple.
8. After reporting on Crowdfind, contact the Center Manager to bring the item(s) to Lost & Found.
