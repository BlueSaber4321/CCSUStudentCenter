# How to report information on 7PointOps

{% embed url="https://www.7pointops.com" %}

1.  Head to [https://www.7pointops.com](https://www.7pointops.com) and log in with the account for the specific center.

    ![](<.gitbook/assets/image (3).png>)![](<.gitbook/assets/image (4).png>)

    * Each center has a different email. Check Page 2 of each SOP for the center login information.
2.  Once you log in, click on Daily Logs

    ![](<.gitbook/assets/image (13).png>)
3.  Scroll down to what center you are operating in. Esports is at the top of the list, while Breakers is halfway down the list. (For this example, we will use Breakers, as I am on shift while doing this.) Click on the "New" button to start a new log.

    ![](<.gitbook/assets/image (25).png>)
4.  When writing your log, Follow this format:

    \[First or Last Name] \[#opening/#update/#closing]

    (Give a brief overview of how the room is doing. Example: "Nothing to report here, everything is kinda slow.") \[Report the Headcount of the room. Example: "Current headcount: 3" or "There are currently only 3 people playing pool, other then that the room is quiet."]
5.  It should end up looking something like this:

    Richards #update

    Nothing to report, everything is good here. Current Headcount: 3

    ![](<.gitbook/assets/image (31).png>)
6.  When you are done, click the "Save" button on the right.

    ![](<.gitbook/assets/image (11).png>)
7.  Scroll back down and check if it shows up. If it is there, you are all set for then.

    ![](<.gitbook/assets/image (21).png>)

### <mark style="color:yellow;">**Be sure to make a log every hour!**</mark>
