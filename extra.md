---
description: Extra Procedures for when you are on shift
---

# Extra Procedures

{% hint style="info" %}
<mark style="color:blue;">**On April 4th, the mask mandate will drop at CCSU. Until then, please keep enforcing the mask policy as normal. After April 4th, masks will be optional, but still recommended.**</mark>

**Masks will remain in their normal locations for people still wanting one.**

<mark style="color:red;">**Food/Drink Policy will still remain in effect!**</mark>
{% endhint %}

This list is the most up to date procedures that you should be following on top of the Standard Operating Procedures.

* Be sure to wipe down all equipment while on shift to slow the spread of COVID!
* Be sure that everyone checks in at the front desk even if they are just visiting.
* <mark style="color:red;background-color:orange;">**Make sure everyone is wearing a mask while in the centers at ALL TIMES!**</mark>
  * <mark style="color:red;background-color:orange;">**If they do not have a mask, there are masks in the box next to the right computer for Breakers, and in the Left Filling Cabinet for Esports.**</mark>
* **No Food is allowed to be eaten in the centers.** Please tell people to eat at one of the locations below:
  * Devli's Den / Semesters
  * Outside
  * Dorm's

There may be more procedures that are being asked of then are listed here. Be sure to check the Teams Group chat for the most up to date information.
