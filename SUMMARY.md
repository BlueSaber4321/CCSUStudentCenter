# Table of contents

* [Home](README.md)
* [How to report information on 7PointOps](7pointops.md)
* [How to report lost items using Crowdfind](crowdfind.md)
* [Extra Procedures](extra.md)
* [BlueTrack](swipe.md)

## Breakers Game Room <a href="#breakers" id="breakers"></a>

* [Breakers Procedures](breakers/procedures/README.md)
  * [Opening](breakers/procedures/opening.md)
  * [Shift Change](breakers/procedures/change.md)
  * [Closing](breakers/procedures/closing.md)
* [Troubleshooting and How-To's](breakers/help/README.md)
  * [Sound System Troubleshooting](breakers/help/sound.md)
  * [How to play Pool (Billiards)](breakers/help/playing-pool.md)
  * [Equipment Checkout](breakers/help/equipment.md)

## Esports Center <a href="#esports" id="esports"></a>

* [Esports Procedures](esports/procedures/README.md)
  * [Opening](esports/procedures/opening.md)
  * [Shift Change](esports/procedures/change.md)
  * [Closing](esports/procedures/closing.md)
* [Shoutcaster Room](esports/shoutcaster/README.md)
  * [Room Rules](esports/shoutcaster/rules.md)
  * [Room Setup](esports/shoutcaster/setup.md)
  * [Setting up the Streaming PC's](esports/shoutcaster/streaming/README.md)
    * [Streaming from the Computers](esports/shoutcaster/streaming/streaming-from-the-computers.md)
    * [Streaming with the Capture Card](esports/shoutcaster/streaming/capturecard.md)
* [Troubleshooting and How-To's](esports/help/README.md)
  * [How to Power On/Off Consoles and the Computers](esports/help/power.md)
  * [PC Information](esports/help/pc-info.md)
  * [General PC Troubleshooting](esports/help/pc-troubleshoot.md)
  * [Checking out controllers to students](esports/help/checkout.md)
  * [How to run Tournaments](esports/help/tournaments.md)
  * [How to use smash.gg](esports/help/smashgg.md)
