# Equipment Checkout

Breakers has multiple different activities that requires different equipment. If a student wants to use a certain activity, they need to come up to the desk to ask. After they ask, you can hand it out, but you need them to check it out on the sign in sheet/BlueTrack. Once they are done, they need to bring them back to the front desk. **It is your responsibility to make sure no one leaves with anything.**

All games must remain behind the desk and cannot remain in the open/in the cabinets with the consoles. If you are a closer, open the cabinets and check if any games are still in the consoles and hide them behind the desk.
