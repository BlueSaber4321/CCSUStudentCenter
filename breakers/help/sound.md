# Sound System Troubleshooting

{% tabs %}
{% tab title="Rack 1" %}
Rack 1: Primary Mixer

DO NOT POWER DOWN ANY SOUND SYSTEM EQUIPMENT EXCEPT THE BLUETOOTH RECIEVER!

The mixer controls the audio output to the entire room. There are little sticky's where the knobs should be for good audio quality.

If for some reason the audio is off, check to make sure the panel is on, by hitting the switch on the far right of the mixer, that should be the power switch. If there is no light above it when it's switched on, contact the operations manager/graduate intern.
{% endtab %}

{% tab title="Rack 2" %}
Rack 2: Bluetooth

Unless you plan to play Bluetooth audio, keep this off. To add a new device, hold down the pairing button.
{% endtab %}

{% tab title="Rack 3" %}
Rack 3: Shure Wireless Microphone Connection

Do not power this down.

The wireless Microphone should be on the same group and channel as what is displayed on the display.
{% endtab %}

{% tab title="Rack 4" %}
Rack 4: Left Main / Right Main Speakers

This is to remain ON

Optimal settings: -19.5dB both sides

Make sure it says "STATUS: OK" at the bottom. If it does not, let the Operations manager know.
{% endtab %}

{% tab title="Rack 5" %}
Rack 5: Subwoofer / Window Speaker

This is to remain ON

Optimal settings: -4.8dB Left and -17.5dB Right

Make sure it says "STATUS: OK" at the bottom. If it does not, let the Operations manager know.
{% endtab %}
{% endtabs %}
