# How to play Pool (Billiards)

{% hint style="success" %}
These are just to allow people to know a general way to play Billiards. Each person has their own way to play, and they are allowed to do so! There isn't one correct way to play!
{% endhint %}

<details>

<summary>Rules of Pool | Read First</summary>

1. Players must choose either solid or striped balls.
   1. The players must sink all their balls before sinking the 8 ball. Once they sink their balls and sink the 8 ball, they win.
2. Players can not sink the 8 ball early. Sinking the 8 ball before sinking the rest of your type means an instant loss.

</details>

1.  To get started with playing pool, you are going to need the following things:

    ![](../../.gitbook/assets/20220212\_134123.jpg)

    1. A rack of balls
    2. A chalk
    3. A triangle
    4. A stick
2.  Take note of the two circles on the table. These are important markers for where to place things.

    ![](../../.gitbook/assets/20220212\_134152.jpg)
3.  Place the triangle and the Cue Ball (The white ball) on each marker. Make sure the triangle is in the middle.

    ![](../../.gitbook/assets/20220212\_134242.jpg)
4.  Remove the 8 ball from the rack of balls and dump the balls into the triangle leaving a space over the marker in the middle.

    ![](../../.gitbook/assets/20220212\_134306.jpg)![](../../.gitbook/assets/20220212\_134338.jpg)
5.  Place the 8 ball in the space in the middle of the triangle.

    ![](../../.gitbook/assets/20220212\_134343.jpg)![](../../.gitbook/assets/20220212\_134446.jpg)
6. At this point your table should look like this. After it is good, remove the triangle without disturbing the balls.
   1. ![](../../.gitbook/assets/20220212\_134423.jpg)![](../../.gitbook/assets/20220212\_134432.jpg)
7. Have each player choose a set of numbers and what type of balls to play.
8. Go ahead and hit the cue ball into the triangle and the balls should spread. Keep playing until you have gone through all your balls, and then you can sink the 8 ball and win.
