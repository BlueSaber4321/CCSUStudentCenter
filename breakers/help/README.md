# Troubleshooting and How-To's

{% content-ref url="sound.md" %}
[sound.md](sound.md)
{% endcontent-ref %}

{% content-ref url="playing-pool.md" %}
[playing-pool.md](playing-pool.md)
{% endcontent-ref %}

{% content-ref url="equipment.md" %}
[equipment.md](equipment.md)
{% endcontent-ref %}
