---
description: The operating Procedures of Breakers Game Room
cover: >-
  https://images.unsplash.com/photo-1545062080-a71640ea75a1?crop=entropy&cs=srgb&fm=jpg&ixid=MnwxOTcwMjR8MHwxfHNlYXJjaHwxfHxwb29sJTIwdGFibGV8ZW58MHx8fHwxNjQ0MDk0NTAz&ixlib=rb-1.2.1&q=85
coverY: 0
---

# Breakers Procedures

#### Please select what type of shift you are on:

{% content-ref url="opening.md" %}
[opening.md](opening.md)
{% endcontent-ref %}

{% content-ref url="change.md" %}
[change.md](change.md)
{% endcontent-ref %}

{% content-ref url="closing.md" %}
[closing.md](closing.md)
{% endcontent-ref %}
