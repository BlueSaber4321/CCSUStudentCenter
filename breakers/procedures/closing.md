---
description: Closing Procedures for Breakers Game Room
---

# Closing

* Punch in and head to Breakers.
* Open the following on the Computers
  * [7PointOps](https://www.7pointops.com)&#x20;
  * Microsoft Teams (Open on right-side computer)
  * [BlueTrack](https://redirect.dylanr.net/bluetrack)
  * Spotify
* Log in to 7Ops with the login for the specific center. The logins are different for Breakers and ESports.
  * Once in 7ops, make an update of the headcount and current status of the room. <mark style="color:orange;">**Be sure to post a new update at the top of each hour!**</mark> [Don't know how? Check here.](../../7pointops.md)
* Once students start coming in, have them sign in at the front desk using either BlueTrack or a paper sign in. If they want any equipment, make sure the note what they take out on the paper, or mark it when they tap on BlueTrack.
* <mark style="color:red;">**While on shift, make sure no one leaves with any equipment that belongs to the center.**</mark> While you are on shift, be sure to [follow this list as well](../../extra.md).
* If at any point during your shift you need to leave your post, radio the CM for cover. <mark style="color:yellow;">**Breakers must be monitored at all times while open!!**</mark>
* When closing, announcements must be made every 30 minutes, 15 minutes, and 5 minutes before the closing time.&#x20;
  * Announce like: Attention Breakers Patrons, Breakers will be closing in (insert time left) minutes. Thank you.
  * If its the final time announcement, be sure to tell people to wrap up their games.
* When closing arrives, all patrons must leave the facility. When everyone has left, cover all the pool tables, turn off the air hockey table, and make sure all the consoles and monitors are turned off.
  * Please also check the cabinets and consoles to make sure no games are left inside.
* When everything is all set, do an inventory check to make sure everything is accounted for.&#x20;
  * If something is missing, report it on 7Ops in your closing report, and let the Operations Manager know via Teams.
  * If everything is accounted for, say in your 7Ops closing report that everything is accounted for, and Breakers is now closed.
* When you are all set, make sure that you are logged out of any personal accounts on the computers and then log out of both computers and Radio the Center manager that you are ready to lock up. Turn off all the lights and head to the front door. The CM will show up and set the alarm.
