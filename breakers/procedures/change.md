---
description: Shift Change Procedures of Breakers Game Room
---

# Shift Change

* Punch in and head to Breakers.
* Open the following on the Computers
  * [7PointOps](https://www.7pointops.com)&#x20;
  * Microsoft Teams (Open on right-side computer)
  * [BlueTrack](https://redirect.dylanr.net/bluetrack)
  * Spotify
* Log in to 7Ops with the login for the specific center. The logins are different for Breakers and ESports.
  * Once in 7ops, make an update of the headcount and current status of the room. <mark style="color:orange;">**Be sure to post a new update at the top of each hour!**</mark> [Don't know how? Check here.](../../7pointops.md)
* Once students start coming in, have them sign in at the front desk using either BlueTrack or a paper sign in. If they want any equipment, make sure the note what they take out on the paper, or mark it when they tap on BlueTrack.
* <mark style="color:red;">**While on shift, make sure no one leaves with any equipment that belongs to the center.**</mark> While you are on shift, be sure to [follow this list as well](../../extra.md).
* If at any point during your shift you need to leave your post, radio the CM for cover. <mark style="color:yellow;">**Breakers must be monitored at all times while open!!**</mark>
* When your shift is almost over, wait until your cover comes. Be sure that if you logged in to any of your personal accounts on the computers that you log out. When your cover arrives, hand them your equipment, log out of the right-side computer, and punch out when your shift ends. If your cover does not arrive, radio the CM for instructions on how to proceed.
