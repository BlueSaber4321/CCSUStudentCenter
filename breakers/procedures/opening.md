---
description: Opening Procedures of Breakers Game Room
---

# Opening

* Punch in and head to the Operations office next to the Info Desk.
* Get the Breakers radio, the keys for the center, and an iPad from the CM (Center Manager)
* Get the CM (Center Manager) to disarm the system for the specific center after they get you the equipment. (If they are not there, radio for them to come to Breakers to disarm.)
* Turn on all the lights and log in at the center Computer behind the desk.
  * Computer Username is `Breakers`. The password is either in the SOP Binder or on a sticky note on the counter. <mark style="color:red;">Do not log in under your Student or Student Center Account.</mark>
* Remove all the pool covers on all the pool tables and place a new sign in sheet on the desk.
* On the computer do the following:
  * On the right computer, log in with your Student Center Account and open teams
  * On the center computer, open [Spotify](https://open.spotify.com), [BlueTrack](https://redirect.dylanr.net/bluetrack), and [7PointOps](https://www.7pointops.com).
    * Both logins are in the SOP's on Page 2
    * Start playing music on Spotify and make a log on 7PointOps that breakers is now open.

The remainder of this list follows the shift change procedure.

Shift Change Procedure:

* Log in to 7Ops with the login for the specific center. The logins are different for Breakers and ESports.
  * Once in 7ops, make an update of the headcount and current status of the room. <mark style="color:orange;">**Be sure to post a new update at the top of each hour!**</mark> [Don't know how? Check here.](../../7pointops.md)
* Once students start coming in, have them sign in at the front desk using either BlueTrack or a paper sign in. If they want any equipment, make sure the note what they take out on the paper, or mark it when they tap on BlueTrack.
* <mark style="color:red;">**While on shift, make sure no one leaves with any equipment that belongs to the center.**</mark> While you are on shift, be sure to [follow this list as well](../../extra.md).
* If at any point during your shift you need to leave your post, radio the CM for cover. <mark style="color:yellow;">**Breakers must be monitored at all times while open!!**</mark>
* When your shift is almost over, wait until your cover comes. Be sure that if you logged into any personal accounts on the computer that you log out. When your cover arrives, hand them your equipment, log out of the computer, and punch out when your shift ends. If your cover does not arrive, radio the CM for instructions on how to proceed.
