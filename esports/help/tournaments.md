# How to run Tournaments

This will show how to run a tournament in the Esports Center.

### Setting up the Tourney

* Open up the page on smash.gg
* Start running a check-in by listing off the attendees and marking them checked in.
  * Announce this using the mic next to the right monitor. Start by saying something like, "Hello everyone, I will be starting check-in, so if you hear your name, please let me know you are here.". After that, start naming off their tags and they will let you know if they are here. You can also ask some of the attendees if they know who is and isnt here.
* After running a check in, if everyone is here you can start the tournament.
* If people are not here, it is your call for if you wish to start anyways and DQ (Disqualify) them, or wait.
* Also, to allow people to see what the bracket status is, throw it up on the back TV's, as well as the stream if there is a stream for this tournament.
  * If there is a stream, they may request access to the shoutcaster room. If needed, check [the how to](../shoutcaster/streaming/) and let them know the [room rules](../shoutcaster/rules.md).

### Running the Tourney

When running the tournament, follow the bracket on smash.gg and queue people onto stations as they become available. Do this by calling people using the Mic next to the right monitor.

* Depending on the tournament, queue people up on every available station if there is a game waiting. This will allow for tournaments to go a lot faster. (If there is a stream occurring, attempt to get as many games on that as possible including the final matches on that monitor.)
  * Announce a station opening as such, "Can I get Player1 and Player2 on Station X" (Replace player1 and 2 and station x with their actual station.
  * Stations are labeled 1-4 for each TV, left to right. Left-most is Station 1, and right-most is Station 4. Most of the time, Station 1 will be the streaming station.
* Be sure during the tournament to report all the scores if possible. When the game finishes, someone will come up to report the games score. If you wish to report the games in Detail, you can using the Report Game Info button in smash.gg (Check the how to use smash.gg page for info)

### Per-Game Extra Information

{% tabs %}
{% tab title="Super Smash Bros. Ultimate" %}
* Each type of Tournament is different, but the general layout is such:
  * General Games are all Best of 3
  * Final Matches are normally Best of 5 (but can be overridden by you if time is permitting.)
* Each games rules will be listed on the Smash.gg page at the bottom.
{% endtab %}
{% endtabs %}

### Ending the Tournament

* After the final match concludes, depending on if there is a prize or not, let them know to contact the tournament organizer.
  * For student center events, have them get in contact with Nick Striefel.
  * For gaming club events, have them get in contact with the gaming club president or listed representative.
* Be sure that the tournament is **not marked** as "In Progress", but should instead show "1st place: Username". If it does show that, you are all set and can close smash.gg
* Be sure no one left anything in shout casters and that the PC's are shut-down. After that, turn off the light and lock the room up.
