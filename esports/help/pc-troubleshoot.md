---
description: '"Just restart it"'
---

# General PC Troubleshooting

### 90% of the issues that occur on the Computers in the Esports Center can be ironed out with a computer restart, but for the 10% of times, please refer to the section below.

#### The PC's are crashing, a game is malfunctioning or was installed incorrectly, or a PC keeps failing an update.

These issues are out of our control and must be reported to IT.&#x20;

Phone: (860) 832-1720

Extension: 2-1720
