---
description: >-
  smash.gg is the program that we use for holding tournaments and events in the
  Esports Center.
---

# How to use smash.gg

First, head to the Smash.gg homepage:

{% embed url="https://smash.gg" %}

<details>

<summary>Creating a SmashGG Tournament</summary>

Click the Organize an event button and then Create a tournament.

![](<../../.gitbook/assets/image (30).png>)![](<../../.gitbook/assets/image (26).png>)

After that, login/create an account and enter the information of the tournament.

![](<../../.gitbook/assets/image (10).png>)

After entering the information you will end up on the Dashboard. This is where you can publish events for viewing.

![](<../../.gitbook/assets/image (19).png>)

Be sure to go through the setup of each section and publish them when done!

When everything is created and you have a registration link, you can send it!

</details>

When you are editing s smash.gg tournament, you will see a sidebar with alot of options:

![](<../../.gitbook/assets/image (24).png>)

| Tab                   | What does it do?                                                                                                                                                                                |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Details               | This tab has all the general information regarding the tournament like starting date/time, contact info, etc.                                                                                   |
| Admin Permissions     | This tab gives other people access to the tournament options and settings. This is good for the OM to view the tournament and make edits if needed.                                             |
| Registration Settings | This will allow people to register for the tournament and to change the time for when registration should close. It is recommended to close 1 hour before the tournament.                       |
| Attendees             | This shows a list of all the attendees and allows you to check them in and remove them if needed.                                                                                               |
| Teams                 | Unless you are running a team event, this is not needed. This allows for you to see who is teamed with who.                                                                                     |
| Events                | This allows for you to create an event in the tournament. At least one event is needed for a tourney to run.                                                                                    |
| Bracket Setup         | Bracket Setup is not needed unless you know what you are doing.                                                                                                                                 |
| Seed Generator        | Allows for you to create a seeding list based on who is the most skilled.                                                                                                                       |
| Pools & Seeding       | Unless you know the current seeding for each player in the tournament, dont use this. You can ask someone that does know to fill it in, or add them as a bracket manager.                       |
| Scheduling            | Not useful to us.                                                                                                                                                                               |
| Conflicts             | Unless you are running multiple events in one Tournament, dont use this.                                                                                                                        |
| Stations & Streams    | Allows for creation of Stations, and assigning streams.                                                                                                                                         |
| Brackets              | Allows for viewing the tournament bracket. This tab is recommended to be stayed on during the tournament to allow for score reporting just by clicking on the match and dropping the game info. |
| Match Dashboard       | View current ongoing matches. Bracket Tab is recommended instead of this.                                                                                                                       |
| Stream Queue          | ?? (Not useful, dont use this)                                                                                                                                                                  |
| Printing              | Printing out match information (dont use this)                                                                                                                                                  |

<details>

<summary>Report Game Information in more Detail</summary>

Some games allow for detailed score reporting. If you know what you are doing and wish to report that information, you can feel free to do so!

By clicking Report Game Info, it will show you a new tab that gives access to how the game was played and a more detailed look into the game without having to check recordings.

</details>
