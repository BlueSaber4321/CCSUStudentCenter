# Troubleshooting and How-To's

{% content-ref url="power.md" %}
[power.md](power.md)
{% endcontent-ref %}

{% content-ref url="pc-info.md" %}
[pc-info.md](pc-info.md)
{% endcontent-ref %}

{% content-ref url="pc-troubleshoot.md" %}
[pc-troubleshoot.md](pc-troubleshoot.md)
{% endcontent-ref %}

{% content-ref url="checkout.md" %}
[checkout.md](checkout.md)
{% endcontent-ref %}

