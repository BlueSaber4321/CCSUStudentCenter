# PC Information

If a Patron asks for the computer information, please feel free to give it to them below:

> ## **General Information**
>
> ### **Computer**
>
> Alienware Aurora R8
>
> ### **Operating System**
>
> Windows 10 Home
>
> ## **CPU Information**
>
> CPU: Intel Core i7-9700K
>
> Clock Speed: 3600MHz
>
> Cores: 8
>
> ## Motherboard
>
> Model: Alienware 0R3FWM
>
> Chipset: Intel Z370 (Kaby Lake)
>
> ## Graphics Cards
>
> Dedicated: (Dell) NVIDIA GeForce RTX 2080 (8192MB VRAM)
>
> Integrated: Intel UHD Graphics 630 (1024MB VRAM) _<- (this likes to accidentally enable sometimes...)_
>
> ## Drives
>
> Boot Disk: TOSHIBA 1024GB (KXG60ZNV1T02) NVMe SSD
>
> Extra Drive: SanDisk SSD Plus 1TB (Attached via SATA, not NVMe)
