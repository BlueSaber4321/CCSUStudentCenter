# How to Power On/Off Consoles and the Computers

{% tabs %}
{% tab title="Alienware Desktops" %}
### Power Off and Power On (At Station)

In the event that the computer disconnected from ggLeap, head to the computer and restart it.

1. Press the Alienware Logo, and the computer should show that it starts shutting down.
2. Press the logo again when all the lights have turned off on the Tower.
3. When it starts up, choose the `eSports` operating system, and then press enter (or wait 30 seconds).

![](../../.gitbook/assets/VideoCapture\_20220209-193724.jpg)![](../../.gitbook/assets/VideoCapture\_20220209-193744.jpg)

![](../../.gitbook/assets/VideoCapture\_20220209-193759.jpg)![](../../.gitbook/assets/VideoCapture\_20220209-193812.jpg)
{% endtab %}

{% tab title="Nintendo Switch" %}
To Power on a Nintendo Switch, press the Power icon on the top left bezel of the Switch. If the Switch is docked, A <mark style="color:green;">green</mark> light will appear when the Switch is turned on.

![](../../.gitbook/assets/20220207\_140230.jpg)![](../../.gitbook/assets/20220207\_140233.jpg)

To power the system off, just tap the button again to have it enter rest mode or click the power icon in the main menu.
{% endtab %}

{% tab title="PlayStation 4" %}
To power on a PS4, press the little button on the rail with a light under the "SONY" logo. To show that the system has turned on, the light will change from <mark style="color:orange;">Orange</mark> to <mark style="color:blue;">Blue</mark>.

![](../../.gitbook/assets/20220207\_140249.jpg)![](../../.gitbook/assets/20220207\_140253.jpg)

To power the system off, press teh button again, or put the system into rest mode by going into the settings menu (Go to the main menu then move the joystick up) then hitting Rest Mode.
{% endtab %}

{% tab title="Xbox One" %}
To power on an Xbox, press the Xbox logo. To show that it has turned on, the logo will also turn on.

![](../../.gitbook/assets/20220207\_140333.jpg)![](../../.gitbook/assets/20220207\_140336.jpg)

To power off the console, either power off the controller by holding the logo, or press the logo on the Xbox.
{% endtab %}
{% endtabs %}
