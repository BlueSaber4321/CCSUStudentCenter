---
description: >-
  Does a student want to play on a Console or a PC using a controller? Here is
  how to hand them out.
---

# Checking out controllers to students

Students may want to play on the consoles in the front of the room, and so it is your responsibility to keep an eye on what is checked out.

When a patron wants to play on a console, you can give them the controller for that specific console. Be sure to keep an eye on it, and make sure they don't leave with it. **Also, make sure you mark the fact that someone has it in BlueTrack, and mark it in inventory of needed.**

Patrons can also ask to use a console with a computer. Be sure to give them a cable to do so.
