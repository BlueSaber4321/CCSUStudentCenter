# Room Rules

* You are only allowed to go into the shoutcaster room while on the clock, unless otherwise stated by the Esports Operations Manager.
* The shoutcaster room is under a closed door policy. Do not let anyone in that does not need to be there.
* This room (as well as Esports) is shared across multiple different departments (IT, The Student Center, and Central Reservations) so do not touch any of the equipment that does not belong to the Student Center.
  * Do not touch any of the server racks. They belong to IT. Contact them if something is wrong regarding them.
  * The computers, chairs, and other things in the room belong to the Student Center, so you can use them if you have a reason behind it.
