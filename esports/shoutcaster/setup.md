# Room Setup

{% hint style="warning" %}
The room is only allowed to be used when it is Reserved, and event is occurring, or if they are allowed by the OM.
{% endhint %}

{% hint style="info" %}
Instructional Photos will be added on Monday.
{% endhint %}

To enter the shoutcaster room, head to the keypad to the right of the door.

Enter the code located at the bottom of Page 2 in the SOP Binder.

Pull then push the door to open it.

Turn on the lights and switch on the Right PC by pressing the Alienware Button.

Sign into the CCSU account with the login in the SOP binder.

The person that reserved the room may be able to handle it from there, but if they need help, [check the setup instructions](streaming/).
