# Shoutcaster Room

The shoutcaster room is a room to stream events and tournaments that occur in the Esports Center.&#x20;

{% content-ref url="rules.md" %}
[rules.md](rules.md)
{% endcontent-ref %}

{% content-ref url="setup.md" %}
[setup.md](setup.md)
{% endcontent-ref %}

{% content-ref url="streaming/" %}
[streaming](streaming/)
{% endcontent-ref %}
