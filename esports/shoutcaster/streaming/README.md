# Setting up the Streaming PC's

{% hint style="danger" %}
There is no way to cover for every possible scenario, so please contact Dylan Richards for assistance with the Computers if needed. If it seems to be a software problem, contact Nick Streifel and he will handle it from there.
{% endhint %}

The computers in Esports are more then capable to run a stream. We have some things all set up for if they want to instantly stream a tournament, but they may require some more configuration. Here is a brief walk-through of how to use OBS. (If you already know, just skip this)

<details>

<summary>What is OBS Studio? [Background]</summary>

OBS Studio, also known as Open Broadcast Software Studio, is a livestreaming and recording program that is one of, if not the most used programs out there for live video streaming (streamlabs sucks).

OBS Studio is basically a drag and drop type of program where you just need to specify what you want the program to actually do.

</details>

Lets get started. Open up OBS Studio and make a new Scene Collection and Profile.

To make a new Scene Collection, click this button on the top bar (check photo below). If you see a collection for what you are already going to do, select that instead. If not, select New and enter what type of game you are using it for.

![](<../../../.gitbook/assets/image (28).png>)

After that, lets select the Profile. Most profiles will be created already, so view below for info:

* For CCSU Student Center Events, Please use the `ESportsCenter` profile.
* For CCSU Gaming Club Events, use the `GamingClub` profile.

After you select the profile, be sure to update the information on the left side.

![](<../../../.gitbook/assets/image (9).png>)

<details>

<summary>There is alot here... Where do I start? [What are Scenes/Sources?]</summary>

OBS may seem very complex at first, but after a little bit, it actually becomes fairly straight-forward.&#x20;

The main thing you need to know Scenes and Sources are. Scenes are basically folders that contain sources. When you swap between Scenes, it will change what sources you have. BE SURE TO LABEL YOUR SCENES! You will get lost if you do not remain organized, so try to keep it simple. If you are just trying to get a video signal out, one scene is perfectly fine.

Click the + button at the bottom left of the scenes section and enter a name.

![](<../../../.gitbook/assets/image (7).png>) This works perfectly fine.

Sources are basically what allows a stream to work. They allow for audio, video and other things to be shown.

Click the + button at the bottom left of the sources section and select a type of source.

![](<../../../.gitbook/assets/image (18).png>)![](<../../../.gitbook/assets/image (22).png>)

Select the type of sources you will need.

* Audio Input Capture: Microphone Capture
* Audio Output Capture: Computer Audio Capture
* Desktop Capture: The computers desktop
* Image: An image
* Video Capture Device: Capture Cards and Webcams

</details>

Depending on what kind of stream it is, it will have different things, so select from below after you finish initial setup:

{% content-ref url="streaming-from-the-computers.md" %}
[streaming-from-the-computers.md](streaming-from-the-computers.md)
{% endcontent-ref %}

{% content-ref url="capturecard.md" %}
[capturecard.md](capturecard.md)
{% endcontent-ref %}
