# Streaming from the Computers

For streaming from the computers, you need to have a computer thats out in the room that you want to get a feed from.

On the crestron next to PC #11, select the "Control Room PC" tab and select what computer you want to be streamed. Head back to the shoutcaster room and find the feed in OBS under video capture devices.

If you want to make some tweaks, feel free.

**If you need more help, feel free to contact Dylan Richards over Teams.**
