# Streaming with the Capture Card

On the first monitor on the left, you will see an HDMI cable that isnt plugged in that feeds under the door into the shoutcaster room. Depending on what console you are using, unplug the cable already there and plug in that cable. (Make sure the shoutcaster room PC is turned on! You won't get a signal if its off!)

In OBS, select Video Capture Device and select the Elgato 4k Capture card. If everything works, you will either get a "No Signal" Feed or a feed of the console.

**If not, please contact Dylan Richards for help over Teams.**
