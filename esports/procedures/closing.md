---
description: Closing Procedures of the Esports Center.
---

# Closing

* Punch in and head to the Esports Center.
* Log in with your Student Center account and open the following on the Computer
  * [7PointOps](https://www.7pointops.com)&#x20;
  * Microsoft Teams
  * [BlueTrack](https://redirect.dylanr.net/bluetrack)
  * [ggLeap](https://admin.ggleap.com)
    * Logins are available on the second page of the SOP's
* Log in to 7Ops with the login for the specific center. The logins are different for Breakers and ESports.
  * Once in 7ops, make an update of the headcount and current status of the room. <mark style="color:orange;">**Be sure to post a new update at the top of each hour!**</mark> [Don't know how? Check here.](../../7pointops.md)
* Once students start coming in, have them sign in at the front desk using either BlueTrack or a paper sign in. If they want any equipment, make sure the note what they take out on the paper, or mark it when they tap on BlueTrack.
* <mark style="color:red;">**While on shift, make sure no one leaves with any equipment that belongs to the center.**</mark> (Have people show the back of their controllers/switches to make sure it's their own personal controllers.) While you are on shift, be sure to [follow this list as well](../../extra.md).
* If at any point during your shift you need to leave your post, radio the CM for cover. <mark style="color:yellow;">**Esports must be monitored at all times while open!!**</mark>
* When closing, announcements must be made every 30 minutes, 15 minutes, and 5 minutes before the closing time.&#x20;
  * Announce like: Attention Esports Center Patrons, the center will be closing in (insert time left) minutes. Thank you.
  * If its the final time announcement, be sure to tell people to wrap up their games.
* When closing arrives, all patrons must leave the facility. When everyone has left, turn off the front and back monitors by going up to the Crestron's and clicking "<mark style="color:red;">EXIT SYSTEM</mark>", and then click Yes. After that, turn off the light switches by the door.
* Move the box that is from behind the desk that contains a bunch of different controllers including JoyCon grips, and an Xbox adaptive controller to the cabinet under Monitor 4
* Check on ggLeap to make sure that all computers are logged out, and check to make sure that nothing is left behind in the room. Check to make sure all the consoles are asleep as well.
* When everything is all set, do an inventory check to make sure everything is accounted for.&#x20;
  * If something is missing, report it on 7Ops in your closing report, and let the Operations Manager know via Teams.
  * If everything is accounted for, say in your 7Ops closing report that everything is accounted for, and Esports is now closed.
* When you are all set, log out of the computer and Radio the Center manager that you are ready to lock up. The CM will show up and set the alarm.
