---
description: Opening Procedures of the Esports Center
---

# Opening



* Punch in and head to the Operations office next to the Info Desk.
* Get the Esports radio and the keys for the center from the CM (Center Manager)
* Get the CM (Center Manager) to disarm the system for the specific center after they get you the equipment. (If they are not there, radio for them to come to Esports to disarm.)
* Turn on all the lights and log in at the computer behind the desk using your Student Center account.
* Dog the front door and the handicap door, and place a doorstop at the main entrance. Turn on the back wall monitors using the Crestron on the pillar next to PC #11 (set it to Digital Signage if it doesn't set automatically), as well as turning on the Crestron by the front door for the console monitors. (<mark style="color:yellow;">Students are not allowed to touch the Crestron by PC #11,</mark> <mark style="color:green;">but they are allowed to use the one by the front door to swap inputs for the consoles.</mark>) If you notice any computers that are off/not working properly, [refer here](../help/pc-troubleshoot.md). Be sure to also turn on the lights if needed. Esports get a lot of natural light during the day, so you don't need to have all the lights on, but at least one set needs to be on.
* On the computer do the following:
  * Open Microsoft Teams, [ggLeap](https://admin.ggleap.com), [BlueTrack](https://redirect.dylanr.net/bluetrack), and [7PointOps](https://www.7pointops.com).
    * 7Ops and ggLeap logins are in the SOP Binder on the second page.
    * Make a log on 7PointOps that Esports is now open.

The remainder of this list follows the shift change procedure.

Shift Change Procedure:

* Log in to 7Ops with the login for the specific center. The logins are different for Breakers and ESports.
  * Once in 7ops, make an update of the headcount and current status of the room. <mark style="color:orange;">**Be sure to post a new update at the top of each hour!**</mark> [Don't know how? Check here.](../../7pointops.md)
* Once students start coming in, have them sign in at the front desk using either BlueTrack or a paper sign in. If they want any equipment, make sure the note what they take out on the paper, or mark it when they tap on BlueTrack.
* <mark style="color:red;">**While on shift, make sure no one leaves with any equipment that belongs to the center.**</mark> (Have people show the back of their controllers/switches to make sure it's their own personal controllers.) While you are on shift, be sure to [follow this list as well](../../extra.md).
* If at any point during your shift you need to leave your post, radio the CM for cover. <mark style="color:yellow;">**Esports must be monitored at all times while open!!**</mark>
* When your shift is almost over, wait until your cover comes.  When your cover arrives, hand them your equipment, log out on the computer, and punch out when your shift ends. If your cover does not arrive, radio the CM for instructions on how to proceed.
