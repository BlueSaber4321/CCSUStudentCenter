---
description: The operating Procedures of the Esports Center
cover: >-
  https://images.unsplash.com/photo-1580327344181-c1163234e5a0?crop=entropy&cs=srgb&fm=jpg&ixid=MnwxOTcwMjR8MHwxfHNlYXJjaHwzfHx2aWRlbyUyMGdhbWV8ZW58MHx8fHwxNjQ0MDk0NDg2&ixlib=rb-1.2.1&q=85
coverY: 0
---

# Esports Procedures

#### Please select what type of shift you are on:

{% content-ref url="opening.md" %}
[opening.md](opening.md)
{% endcontent-ref %}

<details>

<summary>Opening TLDR</summary>

1\.      Punch in, get stuff from Operations office

2\.      Ask Center Manager to disarm esports

3\.      Turn on both Crestrons and set the back TV's to digital signage and remove the box under Monitor 4

4\.      Sign in to the computer and open 7ops and Teams

5\.      Bob the handicap and main door for esports, and put a doorstop at the main door

6\.      Wait for cover, then you can head out when you shift is over.

</details>

{% content-ref url="change.md" %}
[change.md](change.md)
{% endcontent-ref %}

<details>

<summary>Shift Change TLDR</summary>

1\.      Head to the Esports Center and Punch in

2\.      Log into the computer and pull up 7pointops, ggLeap, and Teams

3\.      Post updates every hour on 7PointOps, and make sure no one walks out with Center Equipment.

4\.      Wait for cover, then you can head out when you shift is over.

</details>

{% content-ref url="closing.md" %}
[closing.md](closing.md)
{% endcontent-ref %}

<details>

<summary>Closing TLDR</summary>

1\.      Head to the Esports Center and Punch in

2\.      Log into the computer and pull up 7pointops, ggLeap, and Teams

3\.      Post updates every hour on 7PointOps, and make sure no one walks out with Center Equipment.

4\.      Wait until closing time, while also making announcements near closing time, and close up.

a.      Close up by turning off both crestrons, and consoles.

b.     Move the box with controllers and stuff from behind the desk into the cabinet under Monitor 4 (BE SURE TO LOCK IT)

c.      Send a closing log and shut down

d.     Radio the CM to close up.

</details>
