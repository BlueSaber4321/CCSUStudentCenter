# BlueTrack

{% embed url="http://intranet.ccsu.edu/BlueTrack/login.aspx" %}
BlueTrack Login
{% endembed %}

To access BlueTrack, head to `apps.ccsu.edu` and click on `BlueTrack`.

{% hint style="info" %}
If it is not listed on the apps page, please enter the following in the address bar of the computer: <mark style="background-color:blue;">`redirect.dylanr.net/bluetrack`</mark>.&#x20;

This will directly connect you to the BlueTrack Page.
{% endhint %}

Once you enter the address or click on the app, you will see this page:

![](<.gitbook/assets/image (8).png>)Log in with your Student Center BlueNet ID and Password.

On the next page, it will ask you for a Department, Location, and Event.

![](.gitbook/assets/image.png)

* For Department choose the room you are currently located in.
* The location should automatically be chosen, but if it is not, choose either:
  * Breakers: Breakers Game Room
  * CCSU ESports: ESports room - MH104
* **For Event, enter whatever activity the patron is planning to play. If they are just visiting, etc. Do not enter an event and just click next.**

Next you will end up on this page. This is where the Card gets scanned. **BE SURE THAT YOU ARE ON THE CORRECT WINDOW WHEN THE PATRON SCANS!** To make sure you are, there will be a little black box next to the green arrow. If you see that box, then the patron can scan.

****![](<.gitbook/assets/image (15).png>)****

When someone scans, it will check if they are an active student and if they are, it will display a <mark style="color:green;">**GO**</mark>** ** arrow along with their information and status.

![](<.gitbook/assets/image (5).png>)

The page will go back to allow for scanning after 30 seconds, but if you need to get back immediately, click the Reset button.

![](<.gitbook/assets/image (1).png>)

To go back and enter a different activity, click the back button.

![](<.gitbook/assets/image (2).png>)
